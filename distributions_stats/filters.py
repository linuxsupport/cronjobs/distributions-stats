"""Define Opensearch filters"""

OS_EXISTS = {
  "exists": {
    "field": "data.os_source"
  }
}

APACHE_RESPONSE = {
  "match_phrase": {
      "data.apache_response": 200
    }
  }

REPO_PATH = {
  "bool": {
    "must": [
      {
        "wildcard": {
          "data.path": {
            "value": "*/os/*/repomd.xml"
          }
        }
      }
    ],
    "must_not": [
      {
        "wildcard": {
          "data.path": {
            "value": "*/epel/*"
          }
        }
      }
    ]
  }
}

CERN_IP_ADD = {
  "bool": {
      "minimum_should_match": 1,
      "should": [
        {
          "term": {
            "data.clientip": "10.0.0.0/8"
          }
        },
        {
          "term": {
            "data.clientip": "10.100.0.0/16"
          }
        },
        {
          "term": {
            "data.clientip": "10.254.0.0/16"
          }
        },
        {
          "term": {
            "data.clientip": "10.76.0.0/15"
          }
        },
        {
          "term": {
            "data.clientip": "100.64.0.0/10"
          }
        },
        {
          "term": {
            "data.clientip": "128.141.0.0/16"
          }
        },
        {
          "term": {
            "data.clientip": "128.142.0.0/16"
          }
        },
        {
          "term": {
            "data.clientip": "137.138.0.0/16"
          }
        },
        {
          "term": {
            "data.clientip": "172.16.0.0/12"
          }
        },
        {
          "term": {
            "data.clientip": "185.249.56.0/22"
          }
        },
        {
          "term": {
            "data.clientip": "188.184.0.0/15"
          }
        },
        {
          "term": {
            "data.clientip": "192.168.0.0/16"
          }
        },
        {
          "term": {
            "data.clientip": "192.65.196.0/23"
          }
        },
        {
          "term": {
            "data.clientip": "192.91.242.0/24"
          }
        },
        {
          "term": {
            "data.clientip": "194.12.128.0/18"
          }
        },
        {
          "term": {
            "data.clientip": "2001:1458::/32"
          }
        },
        {
          "term": {
            "data.clientip": "2001:1459::/32"
          }
        },
        {
          "term": {
            "data.clientip": "FD01:1458::/32"
          }
        },
        {
          "term": {
            "data.clientip": "FD01:1459::/32"
          }
        }
      ]
    }
  }


def os_tags_query(os_tags):
    """ Get the query to filter by OS tag"""
    should = []
    for ost in os_tags:
        should.append({"match_phrase": {"data.os_source": ost}})
    os_query = {"bool": { "should": should, "minimum_should_match": 1}}
    return os_query
