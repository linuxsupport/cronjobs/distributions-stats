#!/bin/bash

TOP="/distributions_stats"
UPDINFOPY="/root/web_logs.py"
END_DATE=`/bin/date +%Y-%m-%d`
START_DATE=`/bin/date +%Y-%m-%d --date="$END_DATE - 1 day"`
INDEXNAME_SUMMARY='lxsoft-distributions_stats'
INDEXNAME_ALL='lxsoft-distributions_all_access'

echo $LINUXCI_PWD | kinit $LINUXCI_USER

echo "[landb]" > /root/credentials.ini
echo "landbuser = $LINUXCI_USER" >> /root/credentials.ini
echo "landbpass = $LINUXCI_PWD" >> /root/credentials.ini

echo "$UPDINFOPY --landb --start_date $START_DATE --end_date $END_DATE --type 'all' --path $TOP --indexname_summary $INDEXNAME_SUMMARY --indexname_all $INDEXNAME_ALL"
$UPDINFOPY --landb --start_date $START_DATE --end_date $END_DATE --type 'all' --path $TOP --indexname_summary $INDEXNAME_SUMMARY --indexname_all $INDEXNAME_ALL

echo "done."
