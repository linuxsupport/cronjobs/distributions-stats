#!/usr/bin/python3
""" Utils to get information from LanDB and Opensearch"""
import socket
from datetime import datetime, timedelta, timezone
import os
import json
import time
import sys
import configparser
from suds.client import Client
from suds.sax.element import Element
from suds.xsd.doctor import ImportDoctor, Import
from opensearchpy.helpers import bulk
from filters import os_tags_query
from filters import CERN_IP_ADD, REPO_PATH, APACHE_RESPONSE, OS_EXISTS


def setup_times(timemeasure, timevalue, end_time=None):
    """setup the time range"""
    if end_time:
        endtime = datetime.strptime(end_time, '%Y-%m-%d')
        endtime = endtime + timedelta(microseconds=-1)
    else:
        endtime = datetime.now(timezone.utc)
    if 'since' in timemeasure:
        starttime = datetime.strptime(timevalue, '%Y-%m-%d')
    elif 'days' in timemeasure:
        starttime = endtime-timedelta(days=timevalue)
    elif 'hours' in timemeasure:
        starttime = endtime-timedelta(hours=timevalue)
    else:
        starttime = endtime-timedelta(minutes=timevalue)
    return(starttime, endtime)

def setup_query(os_tags, stime, etime):
    """setup the query from the template"""
    stime = stime.strftime("%Y-%m-%dT%H:%M:%S")
    etime = etime.strftime("%Y-%m-%dT%H:%M:%S")
    if 'all' in os_tags:
        query = {'version': True, 'size': 10000,
             '_source': ['metadata.timestamp', 'data.os_source', 'data.os_arch', 'data.path', 'data.clientip', 'data.puppet_managed', 'data.acctype'], 'query': {
             'bool': {'filter': [OS_EXISTS, APACHE_RESPONSE, REPO_PATH, CERN_IP_ADD,
             {'range': {'metadata.timestamp': {'gte': stime, 'lte': etime, 'format': "yyyy-MM-dd'T'HH:mm:ss"}}}]}}}
    else:
        q_os_tags = os_tags_query(os_tags)
        query = {'version': True, 'size': 10000,
             '_source': ['metadata.timestamp', 'data.os_source', 'data.os_arch', 'data.path', 'data.clientip', 'data.puppet_managed', 'data.acctype'], 'query': {
             'bool': {'filter': [OS_EXISTS, APACHE_RESPONSE, REPO_PATH, CERN_IP_ADD, q_os_tags,
             {'range': {'metadata.timestamp': {'gte': stime, 'lte': etime, 'format': "yyyy-MM-dd'T'HH:mm:ss"}}}]}}}
    return query

def get_data_from_os(client, query):
    """Get data from Opensearch"""
    indexname = 'monit_private_lxsoft_logs_web*'
    search_result = client.search(body=query, index=indexname, size=10000, scroll="3m", timeout=30)
    scroll_id = search_result['_scroll_id']
    scroll_size = len(search_result["hits"]["hits"])  # this is the current 'page' size
    counter = 0
    for _hit in search_result["hits"]["hits"]:
        yield _hit

    while(scroll_size > 0):
        counter += scroll_size
        scroll_result = client.scroll(scroll_id=scroll_id, scroll="1s", timeout=30)
        scroll_id = scroll_result['_scroll_id']
        scroll_size = len(scroll_result['hits']['hits'])
        for _hit in scroll_result["hits"]["hits"]:
            yield _hit
    print(f'Total number of hits: {counter}')


def get_unique_tuples(client, query, puppetmanaged, hosttype, date):
    """ Get the unique tuples (IP, OS, ACCTYPE) from Opensearch
        And a dictionary with the unique ips, with the tuples associated to
    """
    t0 = time.time()
    print('Running Opensearch query')
    dict_tuples = {}
    dict_ips = {}
    arch_list = ['aarch64', 'x86_64', 'ppc64le', 'i386']
    for hit in get_data_from_os(client, query):
        data = hit["_source"]["data"]
        ipadd = data["clientip"]
        puppet_managed = data["puppet_managed"]
        os_arch = data.get("os_arch", None)
        if os_arch not in arch_list:
            os_arch = [i for i in arch_list if i in data['path']]
            if os_arch:
                os_arch = os_arch[0]
            else:
                os_arch = None
        tuple_ip_os_acctype = (ipadd, data["os_source"], data["acctype"])
        if tuple_ip_os_acctype not in dict_tuples.keys():
            if puppet_managed == puppetmanaged or hosttype=='all':
                dict_tuples[tuple_ip_os_acctype] = {'IP': [ipadd],
                                   'PuppetManaged': puppet_managed,
                                   'OS': data["os_source"],
                                   'ARCH': os_arch,
                                   'Requests': 1,
                                   '@timestamp': date,
                                   'ACCTYPE': data["acctype"]}
        else:
            dict_tuples[tuple_ip_os_acctype]['Requests'] += 1
        # Create a dictionary by ip that includes the list of the tuples.
        if ipadd not in dict_ips.keys():
            dict_ips[ipadd] = [tuple_ip_os_acctype]
        else:
            if tuple_ip_os_acctype not in dict_ips[ipadd]:
                dict_ips[ipadd].append(tuple_ip_os_acctype)
    print(f'Total time: {time.time() - t0}')
    print('Dict of Tuples:')
    print(dict_tuples)
    print('Dict of unique IPs:')
    print(dict_ips)
    return dict_tuples, dict_ips

def get_os_per_number_requests(dict_tuples, dict_ips):
    """ Get the OS of the machine based on the max number
        of requests done to linuxsoft repositories
    """
    t0 = time.time()
    dict_result = {}
    for ip in dict_ips:
        ips_tuples = dict_ips[ip]
        max_nrequests = 0
        tuple_ip_win = {}
        for tuple_ip in ips_tuples:
            nrequests = dict_tuples[tuple_ip]['Requests']
            if nrequests > max_nrequests:
                max_nrequests = nrequests
                tuple_ip_win = dict_tuples[tuple_ip]
        dict_result[ip] = tuple_ip_win
    print(f'Total time: {time.time() - t0}')
    print('Dict of unique IPs and their OS:')
    # print(dict_result)
    return dict_result


def get_landb_client():
    """Get LanDB client"""
    # Get user / password
    config_parser = configparser.ConfigParser()
    config_parser.read('credentials.ini')
    username = config_parser['landb']['landbuser']
    password = config_parser['landb']['landbpass']

    # Client setup
    url = 'https://network.cern.ch/sc/soap/soap.fcgi?v=6&WSDL'
    imp = Import('http://schemas.xmlsoap.org/soap/encoding/')
    doc = ImportDoctor(imp)
    client = Client(url, doctor=doc, cache=None)

    # Authentication
    token = client.service.getAuthToken(username, password, 'CERN')
    authenticationHeader = Element('Auth').insert(Element('token').setText(token))
    client.set_options(soapheaders=authenticationHeader)
    return client

def get_landb_owners(cern_ips, dir_path, check_landb_file=False):
    """Get the LanDB owners or groups given a list of IP addresses"""
    t0 = time.time()
    print('Running LanDB')
    landb_dict = {}
    landb_data = {}
    if check_landb_file:
        # read landb data json file
        landb_path = f'{dir_path}/landb_data.json'
        if os.path.isfile(landb_path):
            fl = open(landb_path)
            landb_data = json.load(fl)

    landb_client = get_landb_client()
    for ip_address in cern_ips:
        try:
            fqdn = socket.gethostbyaddr(ip_address)[0]
        except socket.error:
            fqdn = None
        if fqdn:
            if "dyndns.cern.ch" in fqdn:
                shortname = fqdn[:-15]
            elif "ipv6.cern.ch" in fqdn:
                shortname = fqdn[:-13]
            else: # .cern.ch
                shortname = fqdn[:-8]
        else:
            shortname = None

        if shortname is None:
            landb_dict[ip_address] =  {'Hostname': ip_address,
                                        'Department': None,
                                        'Group': None,
                                        'Owner': None,
                                        'Location': None}
        elif shortname in landb_data.keys():
            user_email = landb_data[shortname]['Owner']
            # exclude koji machines
            if user_email == 'koji-admins@cern.ch':
                continue
            if ip_address not in landb_dict:
                landb_dict[ip_address] = landb_data[shortname]
        else:
            # print(f"The machine {shortname} is not in landb info json file, taking the info from landb")
            try:
                landbresult = landb_client.service.getDeviceInfo(shortname)
            except Exception as suds_exception:
                continue
            location = ''
            floor = ''
            room = ''
            if landbresult['Location']['Building'] is not None:
                location = landbresult['Location']['Building']
            if landbresult['Location']['Floor'] is not None:
                floor = landbresult['Location']['Floor']
            if landbresult['Location']['Room'] is not None:
                room = landbresult['Location']['Room']
            department = landbresult['ResponsiblePerson']['Department']
            group = landbresult['ResponsiblePerson']['Group']
            user_email = landbresult['ResponsiblePerson']['Email']

            # exclude koji machines
            if user_email == 'koji-admins@cern.ch':
                continue

            host_info = {'Hostname': shortname,
                         'Department': department,
                         'Group': group,
                         'Owner': user_email,
                         'Location': location+'-'+floor+'-'+room}

            if ip_address not in landb_dict:
                landb_dict[ip_address] = host_info

                if check_landb_file:
                    # add the new machine to landb info file
                    print(f"Adding {shortname} to the json file")
                    landb_data[shortname] = host_info

    if check_landb_file:
        # update landb info jason file
        json_object = json.dumps(landb_data, indent=4)
        with open(f"{dir_path}/landb_data.json", "w") as outfile:
            outfile.write(json_object)
    print(f'Total time: {time.time() - t0}')
    #print('Dict of LanDB info:')
    #print(landb_dict)
    return landb_dict

def aggregate_info(landb_dict, os_dict, tuples=False):
    """Aggregate Openstack and LanDB information"""
    t0 = time.time()
    print('Aggregating OS and LanDB info')
    cern_dict = {}
    if tuples:
        for tuple_ip_os_acc in os_dict:
            ip_os_acc_data = os_dict[tuple_ip_os_acc]
            ipadd = tuple_ip_os_acc[0]
            try:
                landb_data_original = landb_dict[ipadd]
                landb_data = landb_data_original.copy()
            except:
                continue
            hostname = landb_data['Hostname']
            new_tuple = (hostname, tuple_ip_os_acc[1], tuple_ip_os_acc[2])
            if new_tuple in cern_dict.keys():
                cern_dict[new_tuple]['Requests'] += ip_os_acc_data['Requests']
                if ipadd not in cern_dict[new_tuple]['IP']:
                    cern_dict[new_tuple]['IP'].append(ipadd)
            else:
                landb_data.update(ip_os_acc_data)
                cern_dict[new_tuple] = landb_data
    else:
        for ipadd in landb_dict:
            landb_data = landb_dict[ipadd]
            os_data = os_dict[ipadd]
            hostname = landb_data['Hostname']
            if hostname in cern_dict.keys():
                if cern_dict[hostname]['OS'] != os_data['OS'] or cern_dict[hostname]['ACCTYPE'] != os_data['ACCTYPE']:
                    if os_data['Requests'] > cern_dict[hostname]['Requests']:
                        cern_dict[hostname]['Requests'] = os_data['Requests']
                        cern_dict[hostname]['ACCTYPE'] = os_data['ACCTYPE']
                        cern_dict[hostname]['OS'] = os_data['OS']
                else:
                    cern_dict[hostname]['Requests'] += os_data['Requests']
                if ipadd not in cern_dict[hostname]['IP']:
                    cern_dict[hostname]['IP'].append(ipadd)
            else:
                landb_data.update(os_data)
                cern_dict[hostname] = landb_data
    print(f'Total time: {time.time() - t0}')
    #print('Distribution stats info:')
    #print(cern_dict)
    return cern_dict

def bulk_docs(client, data_dict, indexname, number_actions=1000):
    """Bulk data to Opensearch"""
    print('Bulk data to Opensearch')
    t0 = time.time()
    bulk_data = {"_op_type": "create", "_index": indexname}
    actions = []
    for hostname in data_dict.keys():
        bulk_host = bulk_data.copy()
        bulk_host["_source"] = data_dict[hostname]
        actions.append(bulk_host)
    count = 0
    print(f'Number of actions: {len(actions)}')
    while count <= len(actions):
        n_actions = actions[count:count + number_actions]
        success, failed = bulk(client, n_actions, timeout=50)
        print(success, failed)
        if failed:
            print('STOP the process')
            sys.exit(1)
        count = count + number_actions
    print(f'Total time: {time.time() - t0}')
    return

def os_add_doc_index(client, document, indexname):
    """Add a document to the index."""
    response = client.index(
        index=indexname,
        body=document,
        refresh=True,
        timeout=50
    )


if __name__ == '__main__':
    print(get_landb_owners(['188.184.91.48'], False))
