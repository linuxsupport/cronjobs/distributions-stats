#!/usr/bin/python3
"""Allows the processing of web logs."""
import argparse
import sys
import json
from opensearchpy import OpenSearch, RequestsHttpConnection
from requests_kerberos import HTTPKerberosAuth, OPTIONAL
from utils import setup_times, setup_query
from utils import get_landb_owners , get_unique_tuples, get_os_per_number_requests
from utils import aggregate_info, bulk_docs

def parse_args():
    """ define command line arguments"""
    aparser = argparse.ArgumentParser(
        description='Get logs for a distribution repo access')
    aparser.add_argument('--os_tags',
                        help='OS tags',
                        action='store',
                        type=str,
                        nargs='+',
                        default=['all'],
                        dest='os_tags')
    aparser.add_argument('--days',
                         help='days back to search for',
                         action='store',
                         type=int,
                         dest='days')
    aparser.add_argument('--hours',
                         help='hours back to search for',
                         action='store',
                         type=int,
                         dest='hours')
    aparser.add_argument('--minutes',
                         help='minutes back to search for',
                         action='store',
                         type=int,
                         dest='minutes')
    aparser.add_argument('--since',
                         help='date back to search for',
                         action='store',
                         type=str,
                         dest='since')
    aparser.add_argument('--start_date',
                         help='start date back to search for',
                         action='store',
                         type=str,
                         dest='start_date')
    aparser.add_argument('--end_date',
                         help='end date until to search for',
                         action='store',
                         type=str,
                         dest='end_date')
    aparser.add_argument('--landb',
                         help='also query landb to retrieve responsible user for each record',
                         action='store_true',
                         dest='landb')
    aparser.add_argument('--type',
                         help='query puppet,nonpuppet or all types of hosts',
                         default='all',
                         action='store',
                         dest='hosttype')
    aparser.add_argument('--path',
                        help='Directory path to store the file',
                        action='store',
                        dest='dir_path')
    aparser.add_argument('--indexname_summary',
                        help='Indexname to store the entry (IP, OS, ACCTYPE) with more number of requests',
                        action='store',
                        dest='indexname_summary')
    aparser.add_argument('--indexname_all',
                        help='Indexname to store all entries (IP, OS, ACCTYPE)',
                        action='store',
                        dest='indexname_all')
    return aparser.parse_args()


def main():
    """main entry point"""
    args = parse_args()
    try:
        client = OpenSearch(
            ['https://os-linux.cern.ch/os'],
            use_ssl=True,
            verify_certs=True,
            connection_class=RequestsHttpConnection,
            http_auth=HTTPKerberosAuth(mutual_authentication=OPTIONAL)
        )
    except AuthenticationException:
        print("Failed to auth, try kinit?")
        sys.exit(1)

    dict_uip={}

    if args.start_date is not None:
        timemeasure = 'since'
        timevalue = args.start_date
    elif args.since is not None:
        timemeasure = 'since'
        timevalue = args.since
    elif args.days is None and args.hours is None and args.minutes is None:
        timemeasure = 'days'
        timevalue = 1
    elif args.days is not None and args.hours is not None:
        print("both days and hours passed, ignoring hours")
        timemeasure = 'days'
        timevalue = args.days
    elif args.hours is not None and args.minutes is not None:
        print("both hours and minutes passed, ignoring minutes")
        timemeasure = 'hours'
        timevalue = args.hours
    elif args.days is not None:
        timemeasure = 'days'
        timevalue = args.days
    elif args.hours is not None:
        timemeasure = 'hours'
        timevalue = args.hours
    elif args.minutes is not None:
        timemeasure = 'minutes'
        timevalue = args.minutes


    print(f"""Running query from the last {timevalue} {timemeasure}(s)
            (filtering for the OS: {args.os_tags})""")

    if 'nonpuppet' in args.hosttype:
        puppetmanaged = 'false'
    else:
        puppetmanaged = 'true'

    # setup the time ranges
    stime, etime = setup_times(timemeasure, timevalue, args.end_date)

    if args.landb:
        # setup the query file with given time ranges
        query = setup_query(args.os_tags, stime, etime)
        # get the data from Opensearch - tuples (IP, OS, ACCTYPE), and unique IPs
        dict_tuples, dict_ips = get_unique_tuples(client, query, puppetmanaged, args.hosttype, etime.strftime("%Y-%m-%dT%H:%M:%S"))
        print(f"""INFO: There are {len(dict_tuples.keys())} unique tuples
               that have accessed linuxsoft from {stime} to {etime}""")
        print(f"""INFO: There are {len(dict_ips.keys())} unique ips
               that have accessed linuxsoft from {stime} to {etime}""")
        # get the OS of each IP
        cern_ips_os = get_os_per_number_requests(dict_tuples, dict_ips)
        print(f"""INFO: There are {len(cern_ips_os.keys())} unique ips
               that have accessed linuxsoft from {stime} to {etime}""")
        # get landb information
        landb_dict = get_landb_owners(cern_ips_os.keys(), args.dir_path)
        # Agggregate Opensearch info with LanDB info
        data_dict_all = aggregate_info(landb_dict, dict_tuples, tuples=True)
        data_dict_resume = aggregate_info(landb_dict, cern_ips_os)
        # Bulk documents to the index
        bulk_docs(client, data_dict_resume, args.indexname_summary, number_actions=1000)
        bulk_docs(client, data_dict_all, args.indexname_all, number_actions=1000)

    if args.dir_path:
        # Serializing json
        json_object = json.dumps(data_dict_resume, indent=4)
        # Writing to a json file
        with open(f"{args.dir_path}/stats_{str(stime.date())}.json", "w") as outfile:
            outfile.write(json_object)


if __name__ == '__main__':
    sys.exit(main())
