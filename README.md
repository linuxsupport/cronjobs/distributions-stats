# Distributions Stats

* Cronjob that get the daily sucessful requests from CERN IPs to linuxsoft repos for all the distributions provided at CERN per hostname;

    - `prod_distributions_stats`

	- It runs every day at 1 am

* Storing the hostname, ipaddress, department, group, responsible_email, OS, arch, date, #requests, puppet managed and location in Opensearch 

    - `linux_private-lxsoft-stats` index

    - rentention policy of 2 years

* Dashboard

	- https://monit-grafana.cern.ch/d/zkmdcVTSz/distributions-stats?orgId=18