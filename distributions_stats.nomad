job "${PREFIX}_distributions_stats" {
  datacenters = ["*"]

  type = "batch"

  periodic {
    cron = "${SCHEDULE}"
    time_zone = "Europe/Zurich"
    prohibit_overlap = true
  }

  task "${PREFIX}_distributions_stats" {
    driver = "docker"

    config {
      image = "https://gitlab-registry.cern.ch/linuxsupport/cronjobs/distributions-stats/distributions_stats:${CI_COMMIT_SHORT_SHA}"
      logging {
        config {
          tag = "${PREFIX}_distributions_stats"
        }
      }
      volumes = [
        "$DISTRIBUTIONS_STATS:/distributions_stats",
      ]
    }

    env {
      LINUXCI_USER = "$LINUXCI_USER"
      LINUXCI_PWD = "$LINUXCI_PWD"
      TAG = "${PREFIX}_distributions_stats"
    }

    resources {
      cpu = 4000 # Mhz
      memory = 400 # MB
    }

  }
}
